waitVDP:

        LD      A,2
        CALL    GET.VDP.STATUS
        BIT     0,A                     ;check CE bit
        JR      Z,waitEnd
        BIT     7,A                     ;check TR bit
        JR      Z,waitVDP

waitEnd:   LD      A,0
        CALL    GET.VDP.STATUS              ;when exit, you must select S#0
        EI
        RET

GET.VDP.STATUS:                             ;read status register specified by A
        PUSH    BC
        LD      BC,(WRVDP)
        INC     C
        OUT     (C),A
        LD      A,8FH
        OUT     (C),A
        LD      BC,(RDVDP)
        INC     C
        IN      A,(C)
        POP     BC
        RET