
    ;
    ;
    ;   Input:
    ;       HL = origin address
    ;       DE = destination address
    ;       BC = length
    ;       A = origin slot
    ;       A'= destination slot

intslt_LDIR:
    
    push    af
    push    bc
    push    hl
    push    de
    call    RDSLT
    pop de
    ex  de,hl
    ld  e,a ;get value to be written
;    CHANGE_BORDER   e
    ex  af,af'
    push    af
    call    WRSLT
    pop af
    ex  af,af'
    ld  d,h
    ld  e,l
    pop hl
    inc hl
    inc de
    pop bc
    dec bc
    ld  a,c
    or  b
    jr  nz,rep
;    CHANGE_BORDER   0
    pop af
    ret 
rep:
    pop af
    jr  intslt_LDIR

    
    

        