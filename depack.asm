;macro to get a bit from the bitstream
;carry if bit is set, nocarry if bit is clear
;must be entered with second registerset switched in!

	
;In: HL: source
;    DE: destination
depack: inc	hl		;skip original file length
	inc	hl		;which is stored in 4 bytes
	inc	hl
	inc	hl

	ld	a,128
	
	exx
	ld	de,1
	exx
	
depack_loop:
	add	a,a		;shift out new bit
	jp	nz,m01e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m01e:
	jp	c,output_compressed	;if set, we got lz77 compression
	ldi				;copy byte from compressed data to destination (literal byte)
;unrolled for extra speed
	add	a,a		;shift out new bit
	jp	nz,m02e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m02e:
	jp	c,output_compressed	;if set, we got lz77 compression
	ldi				;copy byte from compressed data to destination (literal byte)
	add	a,a		;shift out new bit
	jp	nz,m03e

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m03e:
	jp	c,output_compressed	;if set, we got lz77 compression
	ldi				;copy byte from compressed data to destination (literal byte)
	jp	depack_loop
	

;handle compressed data
output_compressed:
	ld	c,(hl)		;get lowest 7 bits of offset, plus offset extension bit
	inc	hl		;to next byte in compressed data

output_match:
	ld	b,0
	bit	7,c
	jr	z,output_match1	;no need to get extra bits if carry not set

	add	a,a		;shift out new bit
	jp	nz,m04e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m04e:
	rl	b
	add	a,a		;shift out new bit
	jp	nz,m05e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m05e:
	rl	b
	add	a,a		;shift out new bit
	jp	nz,m06e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m06e:
	rl	b
	add	a,a		;shift out new bit
	jp	nz,m07e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m07e:

	jp	c,output_match1	;since extension mark already makes bit 7 set 
	res	7,c		;only clear it if the bit should be cleared
output_match1:
	inc	bc
	
	
;return a gamma-encoded value
;length returned in HL
	exx			;to second register set!
	ld	h,d
	ld	l,e             ;initial length to 1
	ld	b,e		;bitcount to 1

;determine number of bits used to encode value
get_gamma_value_size:
	exx
	add	a,a		;shift out new bit
	jp	nz,m08e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m08e:
	exx
	jr	nc,get_gamma_value_size_end	;if bit not set, bitlength of remaining is known
	inc	b				;increase bitcount
	jp	get_gamma_value_size		;repeat...

get_gamma_value_bits:
	exx
	add	a,a		;shift out new bit
	jp	nz,m09e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m09e:
	exx
	
	adc	hl,hl				;insert new bit in HL
get_gamma_value_size_end:
	djnz	get_gamma_value_bits		;repeat if more bits to go

get_gamma_value_end:
	inc	hl		;length was stored as length-2 so correct this
	exx			;back to normal register set
	
	ret	c
;HL' = length

	push	hl		;address compressed data on stack

	exx
	push	hl		;match length on stack
	exx

	ld	h,d
	ld	l,e		;destination address in HL...
	sbc	hl,bc		;calculate source address

	pop	bc		;match length from stack

	ldir			;transfer data

	pop	hl		;address compressed data back from stack

	add	a,a		;shift out new bit
	jp	nz,m10e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m10e:
	jp	c,output_compressed	;if set, we got lz77 compression
	ldi				;copy byte from compressed data to destination (literal byte)
	add	a,a		;shift out new bit
	jp	nz,m11e		;if remaining value isn't zere, we're done

	ld	a,(hl)		;get 8 bits from bitstream
	inc	hl		;increase source data address

	rla                     ;(bit 0 will be set!!!!)
m11e:
	jp	c,output_compressed	;if set, we got lz77 compression
	ldi				;copy byte from compressed data to destination (literal byte)

	jp	depack_loop
	
	
end