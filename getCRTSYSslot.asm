getCRTslot:
        call    RSLREG
	rrca
	rrca
        and     3       ;00000011B
        ld      b,0
        ld      c,a
        ld      hl,EXPTBL
        add     hl,bc
        ld      b,a
        ld      a,(hl)
        and     $80
        or      b       
        ld      b,a
        inc     hl
        inc     hl
        inc     hl
        inc     hl
        ld      a,(hl)
        and     $0c     ;00001100B
        or      b
	ret
getSYSslot:
        call    RSLREG
	rlca
	rlca
        and     3       ;00000011B
        ld      b,0
        ld      c,a
        ld      hl,EXPTBL
        add     hl,bc
        ld      b,a
        ld      a,(hl)
        and     $80
        or      b       
        ld      b,a
        inc     hl
        inc     hl
        inc     hl
        inc     hl
        ld      a,(hl)
        and     $c0     ;11000000B
	rrca
	rrca
	rrca	
	rrca
        or      b
	ret

